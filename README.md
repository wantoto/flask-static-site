FSS, Flask-Static-Site
==============================================================================
Flask-Static-Site is designed to compile **Jinja2** templates into static web
webpages for static hosting like Amazon AWS S3 and Github Pages.

It also provides a simple development server via **Flask microframework**.


Installation
------------------------------------------------------------------------------
Just `pip install fss`


Layout
------------------------------------------------------------------------------
By default, FSS works with files put as following layout:
```
*--- STATIC-SITE-ROOT    // The root folder of your static site.
  |
  +--- fss_settings.py   // Settings file for FSS
  |
  +--- static            // Static files of this site (Default)
  | +--- index.css
  | +--- index.js
  |
  +--- templates         // Jinja templates of this site (Default)
    +--- index.jinja
    +--- _temp.jinja
    +--- about
      +--- developer.jinja
      +--- designer.jinja
```
FSS compiles above project structure into following layout:
```
*--- STATIC-SITE-ROOT    // The root folder of your static site.
  |
  +--- fss_settings.py   // Settings file for FSS
  |
  +--- static             // Static files of this site
  | +--- index.css
  | +--- index.js
  |
  +--- templates          // Jinja templates of this site
  | +--- index.jinja
  | +--- _temp.jinja
  | +--- about
  |   +--- developer.jinja
  |   +--- designer.jinja
  |
  +--- index.html         // Static html files that static-website can access
  +--- about
    +--- developer.html
    +--- designer.html
```
FSS finds all **Jinja2 templates** in the `templates` folder, compiles it 
into html pages, and puts built result at root directory. *(Templates named with a prefix "\_" won't be compiled)*.


Settings (fss_settings.py)
------------------------------------------------------------------------------
When running `fss` command, `fss` finds `fss_settings.py` in current working
directory. It loads the python module and update settings.
The default settings are:
```python
cwd = os.getcwd()
static_folder = 'static'
templates_folder = 'templates'
templates_context = {}
output_folder = '.'
flask_app_config = {}
```

- **static_folder**
    A folder where to find static files when running a flask dev server and compiling.
    By default, it uses `static` folder under current working directory.

- **templates_folder**
    A folder where to find Jinja2 templates when running a flask dev server and compiling.
    By default, it uses `templates` folder under current working directory.

- **templates_context**
    Context for rendering Jinja2 templates

- **output_folder**
    A folder here to put compiled result.
    By default, it put result in the current working directory.

- **flask_app_config**
    An extra dict used to setup Flask app.



Requirements
------------------------------------------------------------------------------
We use **Flask**, **Jinja2**, and **Frozen-Flask**. Check `requirements.txt`.


Commands
------------------------------------------------------------------------------
* `fss dev_server`
    Start a local development server at http://localhost:5000 via Flask.

* `fss build`
    Compile (render) Jinja2 templates into static html files.


License
------------------------------------------------------------------------------
Copyright 2013 Wantoto Inc http://www.wantoto.com/

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
