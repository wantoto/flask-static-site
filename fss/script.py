#!/usr/bin/env python
#
# Copyright 2013 Wantoto Inc http://www.wantoto.com/
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from fss.flaskr import app as flask_app
from fss.freeze import freezer


#########################
# Build                 #
#########################

def build():
    freezer.freeze()


#########################
# Dev Server            #
#########################

def dev_server():
    flask_app.run()


if __name__ == '__main__':
    import argh

    parser = argh.ArghParser()
    parser.add_commands([build, dev_server])
    parser.dispatch()
